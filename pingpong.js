var ball = {
  x: 320,
  y: 240,
 };

 var level = 1;
 var speed = 2;
 var direction = 2;
 var onBall = false;
 var score1 = 0;
 var score2 = 0;

function setup() {
	createCanvas(640, 480);
}

function draw() {
	background(0);

  var p1y = mouseY;
  var p2y = mouseY;
  var ph = 40;

  //Rebatedores
  fill(245);
	noStroke(255);
	if(mouseX > 0 && mouseX < 320)
    rect(25, p1y, 5, ph, 20);
  else
    rect(25, 220, 5, 40, 20);
       
	if(mouseX > 320)
    rect(615, p2y, 5, ph, 20);
  else
    rect(615, 240, 5, 40, 20);

  //Pontuação
  textSize(20);
  fill(255, 200);
  noStroke();
  text(score1, 280, 30);
  text(score2, 360, 30);

  textSize(18);
  fill(255, 137, 81);
  text("Level " + level, 420, 30);

  //Botão zera placar
    stroke(20);
    strokeWeight(4);
    fill(255, 200);
    if(mouseX > 580 && mouseX < 600 && mouseY > 440 && mouseY < 460)
       fill(255, 0, 0, 200);

    rect(580, 440, 20, 20);
    
    fill(255, 200);
    textSize(14);
    noStroke();
    text("Zerar placar", 485, 455);     
  
  //Bola

  fill(240, 0, 10, 250)
  noStroke();
  ellipse(ball.x, ball.y, 15, 15);

  //Pedeals
  if((ball.y > p1y && ball.y < p1y + ph) && ball.x < 35) {
    
    speed *= -1;
    speed++;
    onBall = true;
    level++;
  }

  if(ball.x < 0) {
    score2++;
    reset();
  }

  if((ball.y > p2y && ball.y < p2y + ph) && ball.x > 605) {
    
    speed *= -1;
    speed--;
    onBall = true;
    level++;
  }

  if(ball.x > 640) {
    score1++;
    reset();
  }

  //Movimentação da bola
  ball.x += speed;

  if(onBall)
    ball.y += direction;

  if(ball.y > 470 || ball.y < 10)
    direction *= -1; 

}

function reset() {
  ball.x = 320;
  ball.y = 240;

  speed *= random(-1, 1);
  if(speed < 0)
    speed = -2;
  else
    speed = 2;

  direction = 2;
  level = 1;
}

function mousePressed() {
  if(mouseX > 580 && mouseX < 600 && mouseY > 440 && mouseY < 460) {
    score1 = 0;
    score2 = 0;
  }
}

/*
function Ball() {
  this.x = random(30, 610);
  this.y = 240;

  this.display = function() {
      fill(240, 0, 10, 250)
      noStroke();
      ellipse(this.x, this.y, 15, 15);   
  }

  this.move = function() {
      this.x += random(-1, 1);
      this.y += random(-1, 1);
  }

}
*/